CCC_ORCA025_LIM/EXP_BULK_IAF_COREv2
===================================

`CCC_ORCA025_LIM` is the CCCma configuration of ORCA0.25

The `EXP_BULK_IAF_COREv2` experiment is a bulk-forced run
with COREv2 inter-annual forcing.

