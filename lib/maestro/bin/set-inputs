#!/bin/bash
set -e
#
# This script takes in a given path and creates symbolic links in ${SEQ_EXP_HOME}/hub/${TRUE_HOST}
# to all files found within it.
#
# Note: this script clears ALL links in the current input directory, if it already exists! 
#       However, real files are maintained as it is assumed these have been placed there
#       by the user.

# check args and environment
path=$1
if [ $# -ne 1 ] ; then
    echo "set-inputs: expects ONE path!"
    echo "set-inputs: exiting ..."
    exit 1
fi
if [ -z "$SEQ_EXP_HOME" ] ; then
    echo "set-inputs: expects SEQ_EXP_HOME to be set in the environment!"
    echo "set-inputs: please set this variable!"
    echo "set-inputs: exiting ..."
    exit 1
fi
if [ -z "$TRUE_HOST" ] ; then
    echo "set-inputs: expects TRUE_HOST to be set in the environment!"
    echo "set-inputs: exiting ..."
    exit 1
fi

# set pertinent variables
work_space=${SEQ_EXP_HOME}/hub/${TRUE_HOST}     # location where input_dir will be created
input_dir="inputs"                              # name of input directory
CWD=`pwd`

# navigate to the workspace on the current host and create inputs directory
cd $work_space
[ -e "$input_dir" ] || mkdir $input_dir

# clear old links
find $input_dir -maxdepth 1 -type l -exec rm -f {} \;
cd $input_dir

# get files within given path and create links
find $path -type f -exec ln -s {} \;

# navigate back to working directory
cd $CWD
